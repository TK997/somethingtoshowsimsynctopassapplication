package com.tk997.androidUI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //feed in the data to my grid view
        ImageAdapter ia = new ImageAdapter(this);
        GridView gv = (GridView)findViewById(R.id.gridView);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
                intent.putExtra("id", ImageAdapter.categories[position]);
                intent.putExtra("name", ImageAdapter.getItemName(position));
                startActivity(intent);

            }
        });


        gv.setAdapter(ia);

    }

    public void customWallpaper(View v){
        Toast.makeText(getApplicationContext(), "Custom wall sticker currently not supported.", Toast.LENGTH_LONG).show();
    }
}
