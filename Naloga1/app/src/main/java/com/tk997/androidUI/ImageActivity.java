package com.tk997.androidUI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        int imgID = getIntent().getExtras().getInt("id");
        Log.d("TAG", "ID: " + imgID);
        ImageView iv =(ImageView) findViewById(R.id.imageView5);

        iv.setImageResource(imgID);

    }
    void openWebSite(View v) {
        Log.d("TAG", "openWebsite");
        int id = getIntent().getExtras().getInt("id");
        Intent intent = new Intent(getApplicationContext(), WebActivity.class);
        intent.putExtra("name", ImageAdapter.getItemName(id));
        startActivity(intent);
    }
}
